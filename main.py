
from tkinter.constants import DISABLED, NORMAL
from skimage.transform import resize
from skimage.metrics import structural_similarity
import cv2
from tkinter import Button, Label, Tk, filedialog
filepath1 = ""
filepath2 = ""

def orb_sim(img1, img2):
    orb = cv2.ORB_create()

    kp_a, desc_a = orb.detectAndCompute(img1, None)
    kp_b, desc_b = orb.detectAndCompute(img2, None)

    bf = cv2.BFMatcher(cv2.NORM_HAMMING, crossCheck=True)

    matches = bf.match(desc_a, desc_b)
    similar_regions = [i for i in matches if i.distance < 50]
    if len(matches) == 0:
        return 0
    return len(similar_regions) / len(matches)


def structural_sim(img1, img2):

    sim, diff = structural_similarity(img1, img2, full=True)
    return sim

def find_similarity(file1,file2):
    img00 = cv2.imread(file1, 0)
    img01 = cv2.imread(file2, 0)

    orb_similarity = orb_sim(img00, img01)
    imgx = resize(img01, (img00.shape[0], img00.shape[1]),anti_aliasing=True, preserve_range=True)
    ssim = structural_sim(img00, imgx) 
    return orb_similarity, ssim

def choose_image(i):
    global filepath1,filepath2

    filePath = filedialog.askopenfilename(title='select', filetypes=[
            ("all image format", ".jpg"),
            ("all image format", ".png"),
            ("all image format", ".jpeg"),
        ])
    if (i==1 and filePath):
        but1["bg"]="green"
        filepath1 = filePath
    if (i==2 and filePath):
        but2["bg"]="green"
        filepath2 = filePath
    
    if(filepath1!="" and filepath2!=""):
        but3["state"] = NORMAL
        but3["bg"] = "#FFC107"
def start():
    orb,ssim = find_similarity(filepath1,filepath2)
    lab1["text"] = "ORB Sim : " + str(int(orb*10000)/100) + "%"
    lab2["text"] = "SSIM Sim : " + str(int(ssim*10000)/100) + "%"
def main():
    global but1, but2, but3, lab1, lab2
    root = Tk()
    root.title("Video Editor by Sudip Mondal")
    root.geometry("300x200")
    root.minsize(300, 200)
    root.maxsize(300, 200)
    root.config(bg="#232323")

    but1 = Button(root, bg="#232323", text= "+", fg="#FFF", font="arial 20 bold", bd=5, command = lambda:choose_image(1))
    but1.pack(side="left",padx="50 0")

    but2 = Button(root, bg="#232323", text= "+",  fg="#FFF", font="arial 20 bold", bd=5, command = lambda:choose_image(2))
    but2.pack(side="right",padx="0 50")

    but3 = Button(root, height=2, width=20, bg="#A3A847",state=DISABLED, text="Check similarity", fg="#FFFFFF", command = lambda:start())
    but3.pack(side="bottom", pady="10")

    lab1 = Label(root, bg = "#232323", text="ORB Similarity", fg="#FFF")
    lab1.pack()
    lab2 = Label(root, bg = "#232323", text="SSIM Similarity", fg="#FFF")
    lab2.pack()

    root.mainloop()

main()